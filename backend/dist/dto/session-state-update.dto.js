"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SessionStateUpdate = void 0;
class SessionStateUpdate {
    constructor(session) {
        this.state = session.state;
        this.connectedUsers = session.connectedUsers;
        this.nUserFinishedProposing = session.nUserFinishedProposing;
        this.nUserFinishedVoting = session.nUserFinishedVoting;
    }
}
exports.SessionStateUpdate = SessionStateUpdate;
//# sourceMappingURL=session-state-update.dto.js.map