import { SessionType } from './create-session.dto';
export declare class TestDto {
    sessionManagerId: string;
    isMultiplePropositionsAllowed: boolean;
    isGroupSizeDefined: boolean;
    groupSize: number;
    problematic?: string;
    sessionType: SessionType;
}
