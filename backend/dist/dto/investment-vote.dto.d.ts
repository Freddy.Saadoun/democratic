declare class InvestmentVote {
    propositionId: string;
    userId: string;
    amount: number;
}
export declare class InvestmentVoteDto {
    votes: InvestmentVote[];
}
export {};
