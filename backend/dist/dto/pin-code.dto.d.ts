export declare class FindSessionWithPinCodeDto {
    userId: string;
    pinCode: string;
}
