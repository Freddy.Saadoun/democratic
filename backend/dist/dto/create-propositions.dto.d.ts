export declare class CreatePropositionsDto {
    propositions: string[];
    userId: string;
    sessionId: string;
}
