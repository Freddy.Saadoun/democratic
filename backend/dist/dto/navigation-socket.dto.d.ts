import { Session } from 'src/entities/session.entity';
export declare class NavigationSocketDto {
    session: Session;
    page: string;
    constructor(session: Session);
    setPageForNextStep(): void;
    setPageForReconnectedUser(userId: string): void;
}
