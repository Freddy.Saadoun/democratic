export declare enum SessionType {
    CLASSIC = "classic",
    INVESTMENT = "investment"
}
export declare class CreateSessionDto {
    sessionManagerId: string;
    isMultiplePropositionsAllowed: boolean;
    isGroupSizeDefined: boolean;
    groupSize: number;
    problematic?: string;
    sessionType: SessionType;
}
