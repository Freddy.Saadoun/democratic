"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TestDto = void 0;
const class_validator_1 = require("class-validator");
const create_session_dto_1 = require("./create-session.dto");
class TestDto {
}
exports.TestDto = TestDto;
__decorate([
    (0, class_validator_1.IsUUID)(),
    __metadata("design:type", String)
], TestDto.prototype, "sessionManagerId", void 0);
__decorate([
    (0, class_validator_1.IsBooleanString)(),
    __metadata("design:type", Boolean)
], TestDto.prototype, "isMultiplePropositionsAllowed", void 0);
__decorate([
    (0, class_validator_1.IsBooleanString)(),
    __metadata("design:type", Boolean)
], TestDto.prototype, "isGroupSizeDefined", void 0);
__decorate([
    (0, class_validator_1.IsNumberString)(),
    __metadata("design:type", Number)
], TestDto.prototype, "groupSize", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], TestDto.prototype, "problematic", void 0);
__decorate([
    (0, class_validator_1.IsEnum)(create_session_dto_1.SessionType),
    __metadata("design:type", String)
], TestDto.prototype, "sessionType", void 0);
//# sourceMappingURL=test.dto.js.map