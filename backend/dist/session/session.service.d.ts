import { DatabaseService } from 'src/database/database.service';
import { QueryRunnerFactory } from 'src/shared/query-runner-factory.service';
import { CreateSessionDto } from 'src/dto/create-session.dto';
import { Session } from 'src/entities/session.entity';
import { FindSessionWithPinCodeDto } from 'src/dto/pin-code.dto';
import { UuidDto } from 'src/dto/uuid.dto';
import { WebsocketGateway } from 'src/websocket/websocket.gateway';
export declare class SessionService {
    private queryRunnerFactory;
    private databaseService;
    private readonly websocketGateway;
    constructor(queryRunnerFactory: QueryRunnerFactory, databaseService: DatabaseService, websocketGateway: WebsocketGateway);
    create(createSessionDto: CreateSessionDto): Promise<Session>;
    findActiveSession(dto: FindSessionWithPinCodeDto): Promise<boolean>;
    findNewPinCode(): Promise<string>;
    nextStep(dto: UuidDto): Promise<void>;
}
