import { Proposition } from './proposition.entity';
import { User } from './user.entity';
export declare class Vote {
    uuid: string;
    amount: number;
    proposition: Proposition;
    author: User;
}
