import { Proposition } from './proposition.entity';
import { User } from './user.entity';
export declare class Session {
    uuid: string;
    pinCode: string;
    multiplePropositions: boolean;
    fixedSize: boolean;
    size: number;
    state: string;
    active: boolean;
    connectedUsers: number;
    nUserFinishedProposing: number;
    nUserFinishedVoting: number;
    type: string;
    problematic: string;
    propositions: Proposition[];
    sessionManager: User;
}
