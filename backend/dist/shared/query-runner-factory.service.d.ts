import { DataSource, EntityManager } from 'typeorm';
export declare class QueryRunnerFactory {
    private dataSource;
    constructor(dataSource: DataSource);
    withTransaction<T>(work: (entityManager: EntityManager) => Promise<T>): Promise<T>;
}
