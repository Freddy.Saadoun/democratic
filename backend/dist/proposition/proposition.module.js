"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PropositionModule = void 0;
const common_1 = require("@nestjs/common");
const proposition_service_1 = require("./proposition.service");
const proposition_controller_1 = require("./proposition.controller");
const shared_module_1 = require("../shared/shared.module");
const database_module_1 = require("../database/database.module");
const websocket_module_1 = require("../websocket/websocket.module");
let PropositionModule = class PropositionModule {
};
exports.PropositionModule = PropositionModule;
exports.PropositionModule = PropositionModule = __decorate([
    (0, common_1.Module)({
        imports: [shared_module_1.SharedModule, database_module_1.DatabaseModule, websocket_module_1.WebsocketModule],
        providers: [proposition_service_1.PropositionService],
        controllers: [proposition_controller_1.PropositionController],
    })
], PropositionModule);
//# sourceMappingURL=proposition.module.js.map