import { Proposition } from 'src/entities/proposition.entity';
import { Session } from 'src/entities/session.entity';
import { User } from 'src/entities/user.entity';
import { Vote } from 'src/entities/vote.entity';
import { EntityManager, Repository } from 'typeorm';
export declare class DatabaseService {
    private sessionRepository;
    private userRepository;
    private propositionRepository;
    constructor(sessionRepository: Repository<Session>, userRepository: Repository<User>, propositionRepository: Repository<Proposition>);
    saveSession(session: Session, entityManager: EntityManager): Promise<Session>;
    updateSession(session: Session, entityManager: EntityManager): Promise<void>;
    findActiveSession(pinCode: string): Promise<Session>;
    findUserByUuid(userId: string): Promise<User>;
    getUserActiveSessions(userId: string): Promise<Session[]>;
    findSessionByUuidWithoutRelations(uuid: string): Promise<Session>;
    findSessionByUuid(uuid: string): Promise<Session>;
    saveUser(user: User): Promise<User>;
    saveClassicVote(vote: Vote, entityManager: EntityManager): Promise<Vote>;
    saveInvestmentVote(votes: Vote[], entityManager: EntityManager): Promise<Vote[]>;
    savePropositions(propositions: Proposition[], entityManager: EntityManager): Promise<Proposition[]>;
    getSessionFromPropositionId(propositionId: string): Promise<Session>;
    addSessionToUser(userId: string, session: Session, entityManager: EntityManager): Promise<void>;
}
