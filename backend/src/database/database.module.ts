import { DatabaseService } from './database.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { User } from 'src/entities/user.entity';
import { Session } from 'src/entities/session.entity';
import { Vote } from 'src/entities/vote.entity';
import { Proposition } from 'src/entities/proposition.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, Session, Vote, Proposition])],
  providers: [DatabaseService],
  exports: [DatabaseService],
})
export class DatabaseModule {}
