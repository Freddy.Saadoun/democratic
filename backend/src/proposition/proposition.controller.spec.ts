import { Test, TestingModule } from '@nestjs/testing';
import { PropositionController } from './proposition.controller';

describe('PropositionController', () => {
  let controller: PropositionController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PropositionController],
    }).compile();

    controller = module.get<PropositionController>(PropositionController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
