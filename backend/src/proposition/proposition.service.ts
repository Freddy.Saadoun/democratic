import { Injectable } from '@nestjs/common';
import { DatabaseService } from 'src/database/database.service';
import { CreatePropositionsDto } from 'src/dto/create-propositions.dto';
import { Proposition } from 'src/entities/proposition.entity';
import { Session } from 'src/entities/session.entity';
import { User } from 'src/entities/user.entity';
import { QueryRunnerFactory } from 'src/shared/query-runner-factory.service';
import { WebsocketGateway } from 'src/websocket/websocket.gateway';
import { EntityManager } from 'typeorm';

@Injectable()
export class PropositionService {
  constructor(
    private queryRunnerFactory: QueryRunnerFactory,
    private databaseService: DatabaseService,
    private readonly websocketGateway: WebsocketGateway,
  ) {}

  async createProposition(dto: CreatePropositionsDto) {
    const session =
      await this.databaseService.findSessionByUuidWithoutRelations(
        dto.sessionId,
      );
    await this.queryRunnerFactory.withTransaction(async (queryRunner) => {
      if (!session.multiplePropositions) {
        dto.propositions = dto.propositions.slice(0, 1);
      }

      const propositions: Proposition[] = dto.propositions.map(
        (proposition) => {
          const propositionEntity = new Proposition();
          propositionEntity.text = proposition;
          propositionEntity.session = { uuid: dto.sessionId } as Session;
          propositionEntity.author = { uuid: dto.userId } as User;
          propositionEntity.totalAmount = 0;
          return propositionEntity;
        },
      );

      await this.databaseService.savePropositions(propositions, queryRunner);

      await this.updateSessionState(session, queryRunner);
    });
  }

  async updateSessionState(session: Session, entityManager: EntityManager) {
    session.nUserFinishedProposing += 1;
    let isNextStep = false;
    if (session.fixedSize && session.nUserFinishedProposing === session.size) {
      session.state = 'voting';
      isNextStep = true;
    }

    await this.databaseService.updateSession(session, entityManager);
    this.websocketGateway.handleWebsocketEvent(isNextStep, session);
  }
}
