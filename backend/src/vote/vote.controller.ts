import { Body, Controller, Post } from '@nestjs/common';
import { VoteService } from './vote.service';
import { ClassicVoteDto } from 'src/dto/classic-vote.dto';
import { InvestmentVoteDto } from 'src/dto/investment-vote.dto';

@Controller('vote')
export class VoteController {
  constructor(private readonly voteService: VoteService) {}

  @Post('classic')
  async classicVote(@Body() vote: ClassicVoteDto) {
    await this.voteService.classicVote(vote);
  }

  @Post('investment')
  async investmentVote(@Body() vote: InvestmentVoteDto) {
    await this.voteService.investmentVote(vote);
  }
}
