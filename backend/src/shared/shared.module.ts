import { Module } from '@nestjs/common';
import { QueryRunnerFactory } from './query-runner-factory.service';

@Module({
  providers: [QueryRunnerFactory],
  exports: [QueryRunnerFactory],
})
export class SharedModule {}
