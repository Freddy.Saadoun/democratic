import { WebsocketModule } from './websocket/websocket.module';
import { QueryRunnerFactory } from './shared/query-runner-factory.service';
import { SharedModule } from './shared/shared.module';
import { DatabaseModule } from './database/database.module';
import { Module } from '@nestjs/common';
import { SessionModule } from './session/session.module';
import { PropositionModule } from './proposition/proposition.module';
import { VoteModule } from './vote/vote.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler';
import { APP_GUARD } from '@nestjs/core';

@Module({
  imports: [
    WebsocketModule,
    SharedModule,
    DatabaseModule,
    SessionModule,
    PropositionModule,
    VoteModule,
    ThrottlerModule.forRoot([
      {
        ttl: 60,
        limit: 10,
      },
    ]),
    TypeOrmModule.forRoot({
      type: 'mariadb',
      host: '0.0.0.0',
      port: 3306,
      username: 'root',
      password: '',
      database: 'democratic',
      autoLoadEntities: true,
      // entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: true,
      dropSchema: false,
      logging: true,
    }),
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: ThrottlerGuard,
    },
    QueryRunnerFactory,
  ],
})
export class AppModule {}
