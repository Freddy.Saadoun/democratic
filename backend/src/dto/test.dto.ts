import {
  IsBooleanString,
  IsEnum,
  IsNumberString,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';
import { SessionType } from './create-session.dto';

export class TestDto {
  @IsUUID()
  sessionManagerId: string;

  @IsBooleanString()
  isMultiplePropositionsAllowed: boolean;

  @IsBooleanString()
  isGroupSizeDefined: boolean;

  @IsNumberString()
  groupSize: number;

  @IsOptional()
  @IsString()
  problematic?: string;

  @IsEnum(SessionType)
  sessionType: SessionType;
}
