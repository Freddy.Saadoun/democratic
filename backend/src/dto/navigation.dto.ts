import { Session } from 'src/entities/session.entity';

export class NavigationDto {
  session: Session;
  page: string;

  constructor(session: Session) {
    this.session = session;
  }

  setPageForNextStep() {
    if (this.session.state === 'voting') {
      if (this.session.type === 'classic') this.page = '/classic-voting-page';
      else this.page = '/investment-voting-page';
    } else if (this.session.state === 'proposing') {
      this.page = '/submit-proposition';
    } else if (this.session.state === 'finished') {
      this.page = '/voting-results-page';
    }
  }

  setPageForReconnectedUser(userId: string) {
    if (this.session.state === 'voting') {
      if (
        this.session.propositions != undefined &&
        this.session.propositions.some((proposition) => {
          if (proposition.votes === undefined) return false;
          return proposition.votes.find((vote) => vote.author.uuid === userId);
        })
      ) {
        this.page = '/waiting-page';
      } else {
        if (this.session.type === 'classic') this.page = '/classic-voting-page';
        else this.page = '/investment-voting-page';
      }
    } else if (this.session.state === 'proposing') {
      if (
        this.session.propositions != undefined &&
        this.session.propositions.some(
          (proposition) => proposition.author.uuid === userId,
        )
      ) {
        this.page = '/waiting-page';
      } else {
        this.page = '/submit-proposition';
      }
    }

    if (
      this.page === '/waiting-page' &&
      this.session.sessionManager.uuid === userId
    ) {
      this.page = '/manager-monitoring-page';
    }
  }
}
