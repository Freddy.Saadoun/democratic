import { IsArray, IsString, IsUUID } from 'class-validator';

export class CreatePropositionsDto {
  @IsArray()
  @IsString({ each: true })
  propositions: string[];

  @IsUUID()
  userId: string;

  @IsUUID()
  sessionId: string;
}
