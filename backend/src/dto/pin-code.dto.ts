import { IsNumberString, IsUUID } from 'class-validator';

export class FindSessionWithPinCodeDto {
  @IsUUID()
  userId: string;

  @IsNumberString()
  pinCode: string;
}
