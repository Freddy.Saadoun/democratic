import { Type } from 'class-transformer';
import { IsArray, IsNumber, IsUUID, ValidateNested } from 'class-validator';

class InvestmentVote {
  @IsUUID()
  propositionId: string;

  @IsUUID()
  userId: string;

  @IsNumber()
  amount: number;
}

export class InvestmentVoteDto {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => InvestmentVote)
  votes: InvestmentVote[];
}
