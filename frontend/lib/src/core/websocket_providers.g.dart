// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'websocket_providers.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$websocketHash() => r'8e7387144ff531637e5599e6388facc783b81ba6';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [websocket].
@ProviderFor(websocket)
const websocketProvider = WebsocketFamily();

/// See also [websocket].
class WebsocketFamily extends Family<AsyncValue<String>> {
  /// See also [websocket].
  const WebsocketFamily();

  /// See also [websocket].
  WebsocketProvider call(
    String uuid,
  ) {
    return WebsocketProvider(
      uuid,
    );
  }

  @override
  WebsocketProvider getProviderOverride(
    covariant WebsocketProvider provider,
  ) {
    return call(
      provider.uuid,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'websocketProvider';
}

/// See also [websocket].
class WebsocketProvider extends AutoDisposeStreamProvider<String> {
  /// See also [websocket].
  WebsocketProvider(
    String uuid,
  ) : this._internal(
          (ref) => websocket(
            ref as WebsocketRef,
            uuid,
          ),
          from: websocketProvider,
          name: r'websocketProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$websocketHash,
          dependencies: WebsocketFamily._dependencies,
          allTransitiveDependencies: WebsocketFamily._allTransitiveDependencies,
          uuid: uuid,
        );

  WebsocketProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.uuid,
  }) : super.internal();

  final String uuid;

  @override
  Override overrideWith(
    Stream<String> Function(WebsocketRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: WebsocketProvider._internal(
        (ref) => create(ref as WebsocketRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        uuid: uuid,
      ),
    );
  }

  @override
  AutoDisposeStreamProviderElement<String> createElement() {
    return _WebsocketProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is WebsocketProvider && other.uuid == uuid;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, uuid.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin WebsocketRef on AutoDisposeStreamProviderRef<String> {
  /// The parameter `uuid` of this provider.
  String get uuid;
}

class _WebsocketProviderElement extends AutoDisposeStreamProviderElement<String>
    with WebsocketRef {
  _WebsocketProviderElement(super.provider);

  @override
  String get uuid => (origin as WebsocketProvider).uuid;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
