import 'package:democratic/src/core/custom_router.dart';
import 'package:democratic/src/core/navigation_provider.dart';
import 'package:democratic/src/core/theme/app_theme.dart';
import 'package:democratic/src/features/proposition_management/presentation/view/proposition_submited_page.dart';
import 'package:democratic/src/features/proposition_management/presentation/view/submit_proposition_page.dart';
import 'package:democratic/src/features/session_management/presentation/view/create_session_page.dart';
import 'package:democratic/src/features/session_management/presentation/view/create_success_page.dart';
import 'package:democratic/src/features/session_management/presentation/view/home_view.dart';
import 'package:democratic/src/features/voting_management/presentation/view/classic_voting_page.dart';
import 'package:democratic/src/features/voting_management/presentation/view/investment_voting_page.dart';
import 'package:democratic/src/features/voting_management/presentation/view/voting_results_page.dart';
import 'package:democratic/src/shared/presentation/manager_monitoring_page.dart';
import 'package:democratic/src/shared/presentation/waiting_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class MyApp extends ConsumerWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final navigatorKey = ref.watch(navigationProvider);
    return MaterialApp(
      navigatorKey: navigatorKey,
      title: 'Flutter Demo',
      theme: appTheme,
      initialRoute: '/',
      routes: {
        '/': (context) => const CustomRouter(),
        '/create-session': (context) => const CreateSessionPage(),
        '/create-session-success': (context) =>
            const CreateSessionSuccessPage(),
        '/submit-proposition': (context) => const SubmitPropositionPage(),
        '/proposition-submited': (context) => const PropositionSubmitedPage(),
        '/waiting-page': (context) => const WaitingPage(),
        '/manager-monitoring-page': (context) => const ManagerMonitoringPage(),
        '/classic-voting-page': (context) => const ClassicVotingPage(),
        '/investment-voting-page': (context) => const InvestmentVotingPage(),
        '/voting-results-page': (context) => const VotingResultsPage(),
      },
    );
  }
}
