import 'package:democratic/src/core/auth/data/user_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserMockProvider implements UserRepository {
  final SharedPreferences prefs;
  UserMockProvider({required this.prefs});
  @override
  Future<String> getUserUuid() async {
    return 'uuid';
  }
}
