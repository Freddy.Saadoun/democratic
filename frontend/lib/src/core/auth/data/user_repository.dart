import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

abstract class UserRepository {
  Future<String> getUserUuid();
}
