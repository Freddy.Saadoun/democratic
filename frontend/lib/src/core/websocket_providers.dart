import 'dart:convert';

import 'package:democratic/src/core/navigation_provider.dart';
import 'package:democratic/src/features/session_management/presentation/state/session_state.dart';
import 'package:democratic/src/shared/state/session_state_info_provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'package:socket_io_client/socket_io_client.dart';

part 'websocket_providers.g.dart';

@riverpod
Stream<String> websocket(WebsocketRef ref, String userId) async* {
  IO.Socket socket = IO.io('http://localhost:3000?userId=$userId',
      IO.OptionBuilder().setTransports(['websocket']).build());
  print('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA');
  socket.onConnect((_) {});

  socket.on('updateSessionStateInfo', (data) {
    final dataDecoded = jsonDecode(data);
    print(dataDecoded);
    ref.read(sessionStateInfoProvider.notifier).updateFromMap(dataDecoded);
    ref.read(sessionNotifierProvider.notifier).addFromMap(dataDecoded);
  });

  socket.on('navigation', (data) {
    final dataDecoded = jsonDecode(data);
    print(dataDecoded);

    if (dataDecoded['session'] != null) {
      final sessionData = dataDecoded['session'] as Map<String, dynamic>;
      ref.read(sessionNotifierProvider.notifier).addFromMap(sessionData);

      if (sessionData['sessionManager']['uuid'] == userId) {
        ref.read(sessionStateInfoProvider.notifier).addFromMap(sessionData);
      }
    }

    if (dataDecoded['page'] != null) {
      ref
          .read(navigationProvider.notifier)
          .navigateTo(dataDecoded['page'] as String);
    }
  });
  socket.onDisconnect((_) => print('disconnect'));
}
