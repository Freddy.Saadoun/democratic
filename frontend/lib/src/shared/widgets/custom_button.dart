import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;
  final Color? backgroundColor;
  final Color textColor;
  final bool active;
  final double? width;
  final bool? isExpanded;

  const CustomButton({
    Key? key,
    required this.text,
    required this.onPressed,
    this.backgroundColor,
    this.textColor = Colors.white,
    this.width,
    this.active = true,
    this.isExpanded = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: isExpanded == true ? double.infinity : width,
      child: ElevatedButton(
        onPressed: onPressed,
        style: ButtonStyle(
          overlayColor:
              active ? null : WidgetStateProperty.all(Colors.grey[400]),
          backgroundColor: WidgetStateProperty.all(backgroundColor ??
              (active ? Colors.grey[800] : Colors.grey[400])),
          shape: WidgetStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
            ),
          ),
          padding: WidgetStateProperty.all(
              const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0)),
          elevation: active
              ? WidgetStateProperty.all(2.0)
              : WidgetStateProperty.all(0.0),
          textStyle: WidgetStateProperty.all(const TextStyle(fontSize: 24)),
        ),
        child: Text(text,
            style: Theme.of(context)
                .textTheme
                .bodyLarge!
                .copyWith(color: textColor)),
      ),
    );
  }
}
