import 'package:flutter/material.dart';

class RadioButtonTile extends StatelessWidget {
  final String title;
  final String? subtitle;
  final String? titleActive;
  final String? subtitleActive;
  final bool isSelected;
  final ValueChanged<bool?> onChanged;

  const RadioButtonTile({
    Key? key,
    required this.title,
    this.subtitle,
    this.titleActive,
    this.subtitleActive,
    required this.isSelected,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onChanged(!isSelected),
      child: ListTile(
        title: Text(
          isSelected ? titleActive ?? title : title,
          style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                fontWeight: FontWeight.w500,
                color: isSelected ? Colors.blue : Colors.black,
              ),
        ),
        subtitle: (isSelected
                ? subtitleActive != null || subtitle != null
                : subtitle != null)
            ? Text(
                isSelected ? subtitleActive ?? subtitle! : subtitle!,
                style: Theme.of(context).textTheme.bodySmall!.copyWith(
                      color: isSelected
                          ? Colors.blue
                          : Colors
                              .black, // Change text color based on isSelected
                    ),
              )
            : null,
        leading: Radio<bool>(
          value: true, // The value this radio represents
          groupValue: isSelected ? true : false, // Current group value
          onChanged: (value) => onChanged(!isSelected),
          toggleable: true,
        ),
      ),
    );
  }
}
