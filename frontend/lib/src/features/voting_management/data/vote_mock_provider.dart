import 'package:democratic/src/features/voting_management/data/vote_repository.dart';
import 'package:democratic/src/features/voting_management/domain/vote.dart';

class VoteMockProvider implements VoteRepository {
  @override
  Future<void> classicVote(String uuid, String idProposition) {
    return Future.value();
  }

  @override
  Future<void> investmentVote(List<Map<String, dynamic>> votes) {
    return Future.value();
  }
}
