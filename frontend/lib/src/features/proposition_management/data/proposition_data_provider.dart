import 'package:democratic/src/features/proposition_management/data/proposition_repository.dart';
import 'package:democratic/src/features/proposition_management/domain/proposition.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:democratic/src/core/constants.dart';

class PropositionDataProvider implements PropositionRepository {
  @override
  Future<void> createPropositions(
      String userId, String sessionId, List<String> propositions) async {
    final response = await http.post(
      Uri.parse('$baseUrl/proposition'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode({
        'userId': userId,
        'sessionId': sessionId,
        'propositions': propositions
      }),
    );
    if (response.statusCode != 201) {
      throw Exception('Failed to create propositions');
    }
  }
}
