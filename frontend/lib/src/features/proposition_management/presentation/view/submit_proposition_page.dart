import 'package:democratic/src/core/navigation_provider.dart';
import 'package:democratic/src/core/providers.dart';
import 'package:democratic/src/features/proposition_management/presentation/state/proposition_state.dart';
import 'package:democratic/src/features/session_management/presentation/state/session_state.dart';
import 'package:democratic/src/shared/state/session_state_info_provider.dart';
import 'package:democratic/src/shared/widgets/app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class SubmitPropositionPage extends ConsumerStatefulWidget {
  const SubmitPropositionPage({super.key});

  @override
  ConsumerState<SubmitPropositionPage> createState() =>
      _SubmitPropositionPageState();
}

class _SubmitPropositionPageState extends ConsumerState<SubmitPropositionPage> {
  @override
  Widget build(BuildContext context) {
    final session = ref.watch(sessionNotifierProvider);
    final propositions = ref.watch(propositionsProvider);

    return Scaffold(
      appBar: CustomAppBar(
        pinCode: session?.pinCode,
      ),
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                session != null && session.problematic != null
                    ? Column(
                        children: [
                          const Text(
                            'Problématique :',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          const SizedBox(height: 20),
                          Text(
                            session.problematic!,
                            style: const TextStyle(
                              fontSize: 16,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          const SizedBox(height: 20),
                        ],
                      )
                    : SizedBox.shrink(),
                Text(
                  (session != null && session.multiplePropositions)
                      ? 'Vos propositions (plusieurs autorisées) :'
                      : 'Votre proposition :',
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 20),
                for (int i = 0; i < propositions.length; i++)
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: TextField(
                      controller: propositions[i],
                      onChanged: (value) {
                        ref
                            .read(propositionsProvider.notifier)
                            .updateProposition(i);
                      },
                      decoration: InputDecoration(
                        hintText: 'Entrez votre proposition',
                        suffixIcon: (session != null &&
                                session.multiplePropositions)
                            ? i != propositions.length - 1
                                ? IconButton(
                                    icon: Icon(Icons.remove),
                                    onPressed: () {
                                      ref
                                          .read(propositionsProvider.notifier)
                                          .removeProposition(i);
                                    },
                                  )
                                : null
                            : null,
                      ),
                    ),
                  ),
                (session != null && session.multiplePropositions)
                    ? propositions.last.text.isNotEmpty
                        ? IconButton(
                            icon: Icon(Icons.add),
                            onPressed: () {
                              ref
                                  .read(propositionsProvider.notifier)
                                  .addProposition();
                            },
                          )
                        : IconButton(
                            onPressed: null,
                            icon: Icon(Icons.add),
                            color: Colors.grey,
                          )
                    : SizedBox.shrink(),
                const SizedBox(height: 20),
                ElevatedButton(
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: const Text('Confirmation'),
                          content: const Text(
                              'Voulez-vous valider vos propositions ?'),
                          actions: [
                            TextButton(
                              onPressed: () async {
                                Navigator.of(context).pop();
                                List<String> propositionsText = propositions
                                    .map((controller) => controller.text)
                                    .where((text) => text.isNotEmpty)
                                    .toList();
                                try {
                                  if (session == null || session.uuid == null) {
                                    throw Exception('Session not found');
                                  }

                                  await ref
                                      .read(myPropositionsProvider.notifier)
                                      .createNewPropositions(
                                          session.uuid!, propositionsText);

                                  if (!mounted) return;

                                  final uuid = ref.read(userIdStateProvider);

                                  if (uuid == null) {
                                    throw Exception('UUID not found');
                                  }

                                  ref
                                      .read(propositionsProvider.notifier)
                                      .disposeControllers();

                                  if (uuid == session.idSessionManager &&
                                      !session.fixedSize) {
                                    ref
                                        .read(navigationProvider.notifier)
                                        .navigateTo('/manager-monitoring-page');
                                  } else {
                                    ref
                                        .read(navigationProvider.notifier)
                                        .navigateTo('/waiting-page');
                                  }
                                } catch (e) {
                                  if (!mounted) return;
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        title: const Text('Error'),
                                        content: Text('$e. Please try again.'),
                                        actions: [
                                          TextButton(
                                            child: const Text('OK'),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      );
                                    },
                                  );
                                }
                              },
                              child: const Text('Confirmer'),
                            ),
                            TextButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: const Text('Annuler'),
                            ),
                          ],
                        );
                      },
                    );
                  },
                  child: Text(
                      propositions.length == 1 && propositions[0].text.isEmpty
                          ? 'Je n\'ai pas de proposition à soumettre'
                          : 'Soumettre'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
