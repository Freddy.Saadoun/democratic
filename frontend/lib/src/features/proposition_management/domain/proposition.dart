import 'dart:convert';

import 'package:flutter/material.dart';

@immutable
class Proposition {
  final String? uuid;
  final String idUser;
  final String text;
  final int totalAmount;

  const Proposition(
      {this.uuid,
      required this.idUser,
      required this.text,
      required this.totalAmount});

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Proposition &&
          runtimeType == other.runtimeType &&
          uuid == other.uuid &&
          idUser == other.idUser &&
          text == other.text &&
          totalAmount == other.totalAmount;

  @override
  int get hashCode =>
      uuid.hashCode ^ idUser.hashCode ^ text.hashCode ^ totalAmount.hashCode;

  @override
  String toString() {
    return 'Proposition{id: $uuid, idUser: $idUser, text: $text, totalAmount: $totalAmount}';
  }

  Proposition copyWith({
    String? id,
    int? idSession,
    String? idUser,
    String? text,
    int? totalAmount,
  }) {
    return Proposition(
      uuid: id ?? this.uuid,
      idUser: idUser ?? this.idUser,
      text: text ?? this.text,
      totalAmount: totalAmount ?? this.totalAmount,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': uuid,
      'idUser': idUser,
      'text': text,
      'totalAmount': totalAmount,
    };
  }

  factory Proposition.fromMap(Map<String, dynamic> map) {
    return Proposition(
      uuid: map['uuid'],
      idUser: map['author']['uuid'],
      text: map['text'],
      totalAmount: map['totalAmount'],
    );
  }

  String toJson() => '''{
    "id": $uuid,
    "idUser": "$idUser",
    "text": "$text",
    "totalAmount": $totalAmount
  }''';

  factory Proposition.fromJson(String source) =>
      Proposition.fromMap(jsonDecode(source));
}
