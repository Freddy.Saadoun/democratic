import 'package:democratic/src/core/navigation_provider.dart';
import 'package:democratic/src/core/providers.dart';
import 'package:democratic/src/features/session_management/presentation/state/session_state.dart';
import 'package:democratic/src/shared/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class HomePage extends ConsumerWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Entrez le code PIN de votre session',
                      style: Theme.of(context).textTheme.headlineMedium!,
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(height: 30),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 50.0),
                      child: PinCodeTextField(
                        keyboardType: TextInputType.number,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        //space between the fields
                        appContext: context,
                        length: 4,
                        obscureText: false,
                        animationType: AnimationType.fade,
                        pinTheme: PinTheme(
                          shape: PinCodeFieldShape.box,
                          borderRadius: BorderRadius.circular(5),
                          fieldHeight: 50,
                          fieldWidth: 40,
                          fieldOuterPadding: const EdgeInsets.all(8),
                          activeFillColor: Colors.white,
                          inactiveColor: Colors.grey,
                          inactiveFillColor: Colors.white,
                        ),
                        animationDuration: const Duration(milliseconds: 300),
                        enableActiveFill: true,
                        onCompleted: (codePin) async {
                          try {
                            bool isValid = false;
                            final userId = ref.read(userIdStateProvider);
                            if (userId == null) {
                              return;
                            }
                            isValid = await ref
                                .read(sessionNotifierProvider.notifier)
                                .fetchSessionFromPin(codePin, userId);
                            if (!isValid) {
                              //Show snackbar
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                  content: Text(
                                    'Votre code pin ne correspond à aucune session active. Veuillez réessayer avec un autre code PIN.',
                                    style: TextStyle(color: Colors.white),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              );
                            }
                          } catch (e) {
                            //TODO: Dialog to show error
                          }
                        },
                        beforeTextPaste: (text) {
                          return true;
                        },
                      ),
                    ),
                    const SizedBox(height: 30),
                    CustomButton(
                        text: "Valider", onPressed: () {}, isExpanded: false),
                  ],
                ),
              ),
              const Divider(),
              Expanded(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CustomButton(
                      text: "Créer nouvelle session",
                      onPressed: () {
                        ref
                            .read(navigationProvider.notifier)
                            .navigateTo('/create-session');
                      },
                      isExpanded: false),
                ],
              )),
              // const Text(
              //   'OU',
              //   style: TextStyle(fontSize: 16.0),
              // ),
              // const SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }
}
