import 'package:democratic/src/core/navigation_provider.dart';
import 'package:democratic/src/features/session_management/domain/session.dart';
import 'package:democratic/src/features/session_management/presentation/state/session_state.dart';
import 'package:democratic/src/shared/state/session_state_info_provider.dart';
import 'package:democratic/src/shared/widgets/custom_bottom_sheet.dart';
import 'package:democratic/src/shared/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CreateSessionSuccessPage extends ConsumerWidget {
  const CreateSessionSuccessPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final session = ref.watch(sessionNotifierProvider);

    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  style: const TextStyle(
                    color: Colors.black,
                  ),
                  children: [
                    const TextSpan(
                        text: 'Votre session a été créée avec succès\n',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold)),
                    const TextSpan(
                        text:
                            'Partagez le code suivant avec les participants\n',
                        style: TextStyle(fontSize: 16)),
                    TextSpan(
                        text: session?.pinCode ?? 'Code introuvable',
                        style: const TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.black)),
                  ],
                ),
              ),
              const SizedBox(height: 30),
              CustomButton(
                text: "Continuer",
                onPressed: () async {
                  final session = ref.read(sessionNotifierProvider);
                  if (session == null) {
                    return;
                  }
                  ref
                      .read(sessionStateInfoProvider.notifier)
                      .add(session.stateInfo);
                  ref
                      .read(navigationProvider.notifier)
                      .navigateTo('/submit-proposition');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
