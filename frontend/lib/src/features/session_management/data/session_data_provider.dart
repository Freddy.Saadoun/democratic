import 'dart:convert';

import 'package:democratic/src/core/constants.dart';
import 'package:democratic/src/features/session_management/data/session_repository.dart';
import 'package:democratic/src/features/session_management/domain/session.dart';
import 'package:http/http.dart' as http;

class SessionDataProvider implements SessionRepository {
  @override
  Future<Session> createSession(Map<String, dynamic> session) async {
    final sessionType = session['sessionType'] as SessionType;
    session.remove('sessionType');
    final requestBody = {
      'sessionType': sessionType.toShortString(),
      ...session,
    };
    final response = await http.post(
      Uri.parse('$baseUrl/session'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(requestBody),
    );
    if (response.statusCode == 201) {
      return Session.fromJson(response.body);
    } else {
      throw Exception('Failed to create session');
    }
  }

  @override
  Future<bool> fetchSessionFromPin(String pinCode, String userId) async {
    print('$baseUrl/session/pin/$pinCode/$userId');
    final response =
        await http.get(Uri.parse('$baseUrl/session/pin/$pinCode/$userId'));
    print(response.body);
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw Exception('Failed to load session');
    }
  }

  @override
  Future<void> nextStep(String uuid) async {
    final response = await http.post(
      Uri.parse('$baseUrl/session/next/$uuid'),
    );
    if (response.statusCode != 201) {
      throw Exception('Failed to go to next step');
    }
  }
}
