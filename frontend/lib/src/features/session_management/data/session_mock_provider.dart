import 'package:democratic/src/features/session_management/data/session_repository.dart';
import 'package:democratic/src/features/session_management/domain/session.dart';
import 'package:democratic/src/features/session_management/domain/session_state_info.dart';

class SessionMockProvider implements SessionRepository {
  final SessionStateInfo sessionStateInfo = const SessionStateInfo(
    state: SessionState.proposing,
    connectedUsers: 2,
    nUserFinishedProposing: 2,
    nUserFinishedVoting: 0,
  );

  @override
  Future<Session> createSession(Map<String, dynamic> session) async {
    return Session(
        uuid: 'uuid',
        pinCode: "1234",
        multiplePropositions: true,
        fixedSize: false,
        size: 5,
        stateInfo: sessionStateInfo,
        voteType: SessionType.classic,
        idSessionManager: "uuid",
        problematic: "Quel est le meilleur film de tous les temps ?");
  }

  @override
  Future<bool> fetchSessionFromPin(String pinCode, String userId) async {
    return true;
  }

  @override
  Future<void> nextStep(String uuid) async {
    return;
  }
}
