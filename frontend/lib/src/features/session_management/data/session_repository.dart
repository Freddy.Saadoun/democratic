import 'package:democratic/src/features/session_management/domain/session.dart';

abstract class SessionRepository {
  Future<Session> createSession(Map<String, dynamic> session);
  Future<bool> fetchSessionFromPin(String pinCode, String userId);
  Future<void> nextStep(String uuid);
}
